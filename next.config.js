const withPlugins = require('next-compose-plugins');
const withOptimizedImages = require('next-optimized-images');
const withCSS = require('@zeit/next-css');
const withSass = require('@zeit/next-sass');
const withBundleAnalyzer = require('@next/bundle-analyzer');

const nextConfig = {
	env: {
		ENV: process.env.NEXT_PUBLIC_ENV,
		API_URL: process.env.NEXT_API_DOCTORALIA
	},
	webpack: (config, { isServer }) => {
		if (isServer) {
			const antStyles = /antd\/.*?\/style\/css.*?/;
			const origExternals = [ ...config.externals ];
			config.externals = [
				(context, request, callback) => {
					if (request.match(antStyles)) return callback();
					if (typeof origExternals[0] === 'function') {
						origExternals[0](context, request, callback);
					} else {
						callback();
					}
				},
				...(typeof origExternals[0] === 'function' ? [] : origExternals)
			];

			config.module.rules.unshift({
				test: antStyles,
				use: 'null-loader'
			});
		}

		config.resolve.alias = {
			...config.resolve.alias
		};

		return config;
	}
};

module.exports = withPlugins(
	[
		withOptimizedImages,
		withSass,
		withCSS,
		[
			withBundleAnalyzer,
			{
				analyzeServer: [ 'server', 'both' ].includes(process.env.BUNDLE_ANALYZE),
				analyzeBrowser: [ 'browser', 'both' ].includes(process.env.BUNDLE_ANALYZE),
				bundleAnalyzerConfig: {
					server: {
						analyzerMode: 'static',
						reportFilename: '../bundles/server.html'
					},
					browser: {
						analyzerMode: 'static',
						reportFilename: '../bundles/client.html'
					}
				}
			}
		]
	],
	nextConfig
);
