module.exports = {
	babelrcRoots: [ '.', 'src/*' ]
};

module.exports = function(api) {
	api.cache(true);

	const presets = [ 'next/babel' ];
	const plugins = [
		[ 'styled-components', { ssr: true, displayName: true, preprocess: false } ],
		[ 'import', { libraryName: 'antd', style: 'css' } ]
	];

	return {
		presets,
		plugins
	};
};
