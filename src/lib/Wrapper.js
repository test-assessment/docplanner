import { useEffect } from 'react';

export const Wrapper = (WrappedComponent) => {
	const Wrappers = (props) => {
		return <WrappedComponent {...props} />;
	};

	Wrappers.getInitialProps = async (ctx) => {
		const componentProps = WrappedComponent.getInitialProps && (await WrappedComponent.getInitialProps(ctx));

		return { ...componentProps };
	};

	return Wrappers;
};
