import { all } from 'redux-saga/effects';

import appointmentSaga from '@shared/redux/appointment/sagas';

export default function* rootSaga(getState) {
	yield all([ appointmentSaga() ]);
}
