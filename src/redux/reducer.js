import { combineReducers } from 'redux';

import App from '@shared/redux/app/reducers';
import appointment from '@shared/redux/appointment/reducers';

export default combineReducers({
	App,
	appointment
});
