import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';

import storage from 'redux-persist/lib/storage';

import createSagaMiddleware, { END } from 'redux-saga';

import rootReducer from './reducer';
import rootSaga from './saga';

const sagaMiddleware = createSagaMiddleware();

const bindMiddleware = (middleware) => {
	if (process.env.NODE_ENV !== 'production') {
		const { composeWithDevTools } = require('redux-devtools-extension');
		return composeWithDevTools(applyMiddleware(middleware));
	}

	return applyMiddleware(middleware);
};

function configureStore(initialState = {}) {
	const isClient = typeof window !== 'undefined';

	const persistConfig = {
		key: 'root',
		storage: storage
	};

	const persistedReducer = persistReducer(persistConfig, rootReducer);

	const store = createStore(isClient ? persistedReducer : rootReducer, initialState, bindMiddleware(sagaMiddleware));

	store.runSaga = () => {
		if (store.saga) return;

		store.saga = sagaMiddleware.run(rootSaga);
	};

	store.stopSaga = async () => {
		if (!store.saga) return;

		store.dispatch(END);

		await store.saga.done;

		store.saga = null;
	};

	store.execSagaTasks = async (isServer, tasks) => {
		store.runSaga();

		tasks(store.dispatch);

		await store.stopSaga();

		if (!isServer) {
			store.runSaga();
		}
	};

	store.runSaga();

	if (isClient) {
		store.__PERSISTOR = persistStore(store);
	}

	return store;
}

export default configureStore;
