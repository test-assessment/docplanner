// For moment.js. Some names aren't capitalized
export const esMonths = 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_');
export const esWeeks = 'Domingo_Lunes_Martes_Miércoles_Jueves_Viernes_Sábado'.split('_');
