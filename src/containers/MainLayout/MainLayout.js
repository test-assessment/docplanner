import React from 'react';
import { Layout } from 'antd';

import AppHolder from './MainLayout.styles';

const { Content, Footer } = Layout;

export default function MainLayout({ children }) {
	return (
		<AppHolder>
			<Layout style={{ height: '100%' }}>
				<Layout
					className="contentMainLayout"
					style={{
						height: '100%',
						backgroundColor: '#eef2f2'
					}}
				>
					<Content className="initialLayoutContent">{children}</Content>
				</Layout>
			</Layout>
		</AppHolder>
	);
}
