import styled from 'styled-components';

const AppHolder = styled.div`
	.initialLayoutContent {
		width: 100%;
		background-color: #eef2f2;
	}
`;

export default AppHolder;
