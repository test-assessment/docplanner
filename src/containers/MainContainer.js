import React from 'react';
import { useSelector } from 'react-redux';

import { ConfigProvider } from 'antd';
import { IntlProvider } from 'react-intl';
import { ThemeProvider } from 'styled-components';
import { esMonths, esWeeks } from '../config/esLang';

import themeDefault from '@shared/config/theme/theme.config';
import AppLocale from '@shared/config/translation';

import moment from 'moment';

export default function AppProvider({ children }) {
	const { lang } = useSelector((state) => state.App);

	const newLang = lang || process.env.NEXT_PUBLIC_APP_LANG || 'en';

	const currentAppLocale = AppLocale[newLang];

	moment.updateLocale('es', { months: esMonths, weekdays: esWeeks });
	moment.locale(newLang);

	return (
		<ConfigProvider locale={currentAppLocale.antd}>
			<IntlProvider locale={currentAppLocale.locale} messages={currentAppLocale.messages}>
				<ThemeProvider theme={themeDefault}>{children}</ThemeProvider>
			</IntlProvider>
		</ConfigProvider>
	);
}
