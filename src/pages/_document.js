import Document, { Html, Head, Main, NextScript } from 'next/document';
import { ServerStyleSheet } from 'styled-components';

export default class CustomDocument extends Document {
	static async getInitialProps(ctx) {
		const sheet = new ServerStyleSheet();
		const originalRenderPage = ctx.renderPage;

		try {
			ctx.renderPage = () =>
				originalRenderPage({
					enhanceApp: (App) => (props) => sheet.collectStyles(<App {...props} />)
				});

			const initialProps = await Document.getInitialProps(ctx);

			return {
				...initialProps,
				styles: (
					<React.Fragment>
						{initialProps.styles}
						{sheet.getStyleElement()}
					</React.Fragment>
				)
			};
		} finally {
			sheet.seal();
		}
	}

	render() {
		return (
			<Html>
				<Head>
					<link rel="stylesheet" href="/static/global.css" />
					<link
						href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700"
						rel="stylesheet"
						async
					/>
				</Head>
				<body style={{ backgroundColor: '#eef2f2' }}>
					<Main />
					<NextScript />

					<script type="module" src="https://unpkg.com/ionicons@5.1.2/dist/ionicons/ionicons.esm.js" />
					<script noModule="" src="https://unpkg.com/ionicons@5.1.2/dist/ionicons/ionicons.js" />
				</body>
			</Html>
		);
	}
}
