import React from 'react';
import { Wrapper } from '../lib/Wrapper';

import MainLayout from '../containers/MainLayout/MainLayout';

import Main from '@shared/containers/Main/Main';

export default Wrapper(() => (
    <>
		<MainLayout>
			<Main />
		</MainLayout>
    </>
));

