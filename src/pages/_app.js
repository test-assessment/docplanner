import React from 'react';

import App from 'next/app';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import withRedux from 'next-redux-wrapper';
import MainContainer from '../containers/MainContainer';
import initStore from '../redux/store';
import Head from 'next/head';

import 'antd/dist/antd.css';

class MainApp extends App {
	render() {
		const { Component, pageProps, store } = this.props;

		return (
			<Provider store={store}>
				<PersistGate loading={null} persistor={store.__PERSISTOR}>
					<MainContainer>
						<Head>
							<meta
								name="viewport"
								content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
							/>
						</Head>
						<Component {...pageProps} />
					</MainContainer>
				</PersistGate>
			</Provider>
		);
	}
}

export default withRedux(initStore)(MainApp);
