import React from 'react';

import LayoutContent from '@shared/components/UI/elements/LayoutContent';

import AppointmentContainer from '@shared/components/Appointment/AppointmentContainer';

export default function Main(props) {
	return (
		<LayoutContent>
			<AppointmentContainer />
		</LayoutContent>
	);
}
