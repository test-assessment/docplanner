import defaultTheme from './default';

export const themeConfig = {
	theme: 'defaultTheme'
};

export default defaultTheme;
