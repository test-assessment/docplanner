const theme = {};

theme.palette = {
	primary: [ '#4482FF', '#3A78F5' ],
	secondary: [ '#2d3446', '#f1f3f6', '#788195' ],
	color: [ '#FEAC01', '#42299a', '#F75D81' ],
	warning: [ '#ffbf00' ],
	success: [ '#00b16a' ],
	error: [ '#f64744', '#EC3D3A', '#FF5B58' ]
};

theme.fonts = {
	primary: 'Roboto, sans-serif',
	pre: 'Consolas, Liberation Mono, Menlo, Courier, monospace'
};

export default theme;
