import EnLang from './entries/en-US';
import EsLang from './entries/es-ES';

const AppLocale = {
	en: EnLang,
	es: EsLang
};

export default AppLocale;
