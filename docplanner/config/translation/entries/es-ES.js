import antdEs from 'antd/lib/locale-provider/es_ES';
import esMessages from '../locales/es_ES.json';

const EsLang = {
	messages: {
		...esMessages
	},
	antd: antdEs,
	locale: 'es-ES'
};

export default EsLang;
