import isEmpty from 'lodash/isEmpty';

const base = (method, url, data = {}) => {
	try {
		let payload = {
			method: method.toUpperCase(),
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json'
			}
		};

		if (!isEmpty(data)) {
			payload.body = JSON.stringify(data);
		}

		return fetch(url, payload)
			.then((res) => res.text())
			.then((text) => (text.length ? JSON.parse(text) : true))
			.catch((error) => {
				return {
					statusCode: 500,
					error: error
				};
			});
	} catch (e) {}
};

const sFetch = {};

[ 'get', 'post', 'put', 'delete', 'patch' ].forEach((method) => {
	sFetch[method] = base.bind(null, method);
});

export default sFetch;
