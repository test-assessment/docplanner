export const decodeDate = (date) => {
	try {
		return date.slice(0, -1).split('T');
	} catch (err) {
		console.log(`Date parse fail: ${err}`);

		return false;
	}  
}; 

export const weekDaysNamelist = () => {
	console.log(intl.formatMessage({ id: 'days.sunday' }).split('_'));
	return [
		intl.formatMessage({ id: 'days.sunday' }),
		intl.formatMessage({ id: 'days.monday' }),
		intl.formatMessage({ id: 'days.tuesday' }),
		intl.formatMessage({ id: 'days.wednesday' }),
		intl.formatMessage({ id: 'days.thursday' }),
		intl.formatMessage({ id: 'days.friday' }),
		intl.formatMessage({ id: 'days.saturday' })
	];
};
