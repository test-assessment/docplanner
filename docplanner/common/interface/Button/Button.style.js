import styled from 'styled-components';
import { borderRadius } from '@shared/common/lib/helpers/styleHelper';

const ButtonWrapper = (ComponentName) => styled(ComponentName)`
    color: #FFF;
    background-color: #27AE60;
    border: 0;
    width: 140px;
    height: 40px;
    font-size: 16px;
    font-weight: bold;

    ${borderRadius('4px')};
    
    &:hover, &:focus, &:active {
        background-color: #4fc380;
        color: #FFF;
    }

`;

export { ButtonWrapper };
