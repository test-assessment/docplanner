import { Button } from 'antd';

import { ButtonWrapper } from './Button.style';

const StyledButton = ButtonWrapper(Button);

export default StyledButton;
