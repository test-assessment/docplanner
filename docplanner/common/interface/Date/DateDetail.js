import React from 'react';

import { Dates } from './Date.styles';

const DateDetail = ({ title, date }) => {
	return (
		<Dates>
			<div className="title">{title || ''}</div>
			<div className="description">{date || ''}</div>
		</Dates>
	);
};

export default DateDetail;
