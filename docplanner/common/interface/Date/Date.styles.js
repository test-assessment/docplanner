import styled from 'styled-components';

const Dates = styled.div`
	.title {
		color: #4e4e4e;
		font-size: 13px;
	}

	.description {
		font-weight: 500;
		font-size: 15px;
	}
`;

export { Dates };
