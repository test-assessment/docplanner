import { Select } from 'antd';
import { AntSelect } from './Select.style';

const StyledSelect = AntSelect(Select);

const SelectOption = Select.Option;

export default StyledSelect;

export { SelectOption };
