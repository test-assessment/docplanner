import produce from 'immer';
import actions from './actions';

const initState = {
	isLoading: false,
	errorMessage: false,
	modalActive: false,
	appointment: {},
	slot: []
};

const appointmentReducer = (state = initState, { type, payload }) =>
	produce(state, (draft) => {
		switch (type) {
			case actions.FETCH_SLOT:
				draft.isLoading = true;
				draft.errorMessage = false;
				break;
			case actions.FETCH_SLOT_SUCCESS:
				draft.isLoading = false;
				draft.slot = payload.data;
				break;
			case actions.FETCH_SLOT_ERROR:
				draft.errorMessage = payload.data;
				draft.isLoading = false;
				break;
			case actions.SELECT_APPOINTMENT:
				draft.appointment = payload.data;
				draft.modalActive = true;
				break;
			case actions.BOOK_APPOINTMENT:
				draft.isLoading = true;
				break;
			case actions.BOOK_APPOINTMENT_SUCCESS:
				draft.isLoading = false;
				draft.modalActive = false;
				break;
			case actions.BOOK_APPOINTMENT_ERROR:
				draft.isLoading = false;
				draft.modalActive = false;
				draft.errorMessage = payload.data;
				draft.appointment = initState.appointment;
				break;
			case actions.MODAL_HANDLE:
				draft.modalActive = payload.data === true;
				draft.appointment = initState.appointment;
				break;
			case actions.RESET_STATE:
				return initState;
		}
	});

export default appointmentReducer;
