import produce from 'immer';

import appointmentReducer from '../reducers';
import actions from '../actions';

/* eslint-disable default-case, no-param-reassign */
describe('Appointment Reducer', () => {
	let state;

	beforeEach(() => {
		state = {
			isLoading: false,
			errorMessage: false,
			modalActive: false,
			appointment: {},
			slot: []
		};
	});

	it('Should return the initial state', () => {
		const expectedResult = state;

		expect(appointmentReducer(undefined, {})).toEqual(expectedResult);
	});

	it('Should handle the modal open', () => {
		const expectedResult = produce(state, (draft) => {
			draft.modalActive = true;
		});

		expect(appointmentReducer(state, actions.setModal(true))).toEqual(expectedResult);
	});

	it('Should handle the modal close', () => {
		const expectedResult = produce(state, (draft) => {
			draft.modalActive = false;
		});

		expect(appointmentReducer(state, actions.setModal(false))).toEqual(expectedResult);
	});

	it('Should store appointment in reducer', () => {
		const appointment = {
			Start: '2020-08-26 12:25:30',
			End: '2020-08-26 13:25:30',
			Comments: 'Information for Doc.',
			Patient: {
				Name: 'John',
				SecondName: 'Perez',
				Email: 'john@email.com',
				Phone: '566445323'
			}
		};

		const expectedResult = produce(state, (draft) => {
			draft.modalActive = true;
			draft.appointment = appointment;
		});

		expect(appointmentReducer(state, actions.selectAppointment(appointment))).toEqual(expectedResult);
	});

	it('Should set state for saga in reducer', () => {
		const expectedResult = produce(state, (draft) => {
			draft.isLoading = true;
		});

		expect(appointmentReducer(state, actions.bookAppointment())).toEqual(expectedResult);
	});

	it('Should fetch weekly slots ', () => {
		const date = '20200826';

		const expectedResult = produce(state, (draft) => {
			draft.isLoading = true;
		});

		expect(appointmentReducer(state, actions.fetchWeeklySlot(date))).toEqual(expectedResult);
	});
});
