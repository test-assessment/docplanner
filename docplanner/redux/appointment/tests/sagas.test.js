import { all, takeEvery } from 'redux-saga/effects';
import actions from '../actions';

import rootSaga, { getSlots, saveAppointment } from '../sagas';

/* eslint-disable default-case, no-param-reassign redux-saga/yield-effects */
describe('Appointment Saga', () => {
	let testDate = '20200924';
	let genSlots;

	beforeEach(() => {
		genSlots = getSlots({ date: testDate, forwardWeeks: 1 });

		const selectDescriptor = genSlots.next().value;
		expect(selectDescriptor).toMatchSnapshot();

		const callDescriptor = genSlots.next(testDate).value;
		expect(callDescriptor).toMatchSnapshot();
	});
});

describe('Test appointment root saga', () => {
	const mainRootSaga = rootSaga();

	it('Should start task to watch for FETCH_SLOT and BOOK_APPOINTMENT action', () => {
		const result = mainRootSaga.next().value;
		const expected = all([
			takeEvery(actions.FETCH_SLOT, getSlots),
			takeEvery(actions.BOOK_APPOINTMENT, saveAppointment)
		]);

		expect(result).toEqual(expected);
	});
});
