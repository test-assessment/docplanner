import actions from '../actions';

describe('Appointment Actions', () => {
	describe('Handle weekly slot', () => {
		it('Should return the correct type', () => {
			const expectedResult = {
				type: actions.FETCH_SLOT,
				payload: {
					data: null
				}
			};

			expect(actions.fetchWeeklySlot()).toEqual(expectedResult);
		});

		it('Should return the correct date', () => {
			const testDate = '20200824';

			const expectedResult = {
				type: actions.FETCH_SLOT,
				payload: {
					data: testDate
				}
			};

			expect(actions.fetchWeeklySlot(testDate)).toEqual(expectedResult);
		});

		it('Should return the correct date', () => {
			const mock = {
				Start: '2020-08-24T09:00:00',
				End: '2020-08-24T09:10:00',
				Taken: true
			};

			const expectedResult = {
				type: actions.FETCH_SLOT_SUCCESS,
				payload: {
					data: mock
				}
			};

			expect(actions.fetchWeeklySlotSuccess(mock)).toEqual(expectedResult);
		});
	});

	describe('Handle appointments', () => {
		it('Should save in store an appointment', () => {
			const appointment = {
				Start: '2020-08-26 12:25:30',
				End: '2020-08-26 13:25:30',
				Comments: 'Information for Doc.',
				Patient: {
					Name: 'John',
					SecondName: 'Perez',
					Email: 'john@email.com',
					Phone: '566445323'
				}
			};

			const expectedResult = {
				type: actions.SELECT_APPOINTMENT,
				payload: {
					data: appointment
				}
			};

			expect(actions.selectAppointment(appointment)).toEqual(expectedResult);
		});
	});
});
