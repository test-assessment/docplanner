const DOCUMENT = 'APPOINTMENT_';

const actions = {
	FETCH_SLOT: DOCUMENT + 'FETCH_SLOT',
	FETCH_SLOT_SUCCESS: DOCUMENT + 'FETCH_SLOT_SUCCESS',
	FETCH_SLOT_ERROR: DOCUMENT + 'FETCH_SLOT_ERROR',

	SELECT_APPOINTMENT: DOCUMENT + 'SELECT_APPOINTMENT',
	BOOK_APPOINTMENT: DOCUMENT + 'BOOK_APPOINTMENT',
	BOOK_APPOINTMENT_SUCCESS: DOCUMENT + 'BOOK_APPOINTMENT_SUCCESS',
	BOOK_APPOINTMENT_ERROR: DOCUMENT + 'BOOK_APPOINTMENT_ERROR',

	FETCH_FINISH: DOCUMENT + 'FETCH_FINISH',
	RESET_STATE: DOCUMENT + 'RESET_STATE',
	CHANGE_DATA_ORDER: DOCUMENT + 'CHANGE_DATA_ORDER',

	MODAL_HANDLE: DOCUMENT + 'MODAL_HANDLE',
	MODAL_FILL: DOCUMENT + 'MODAL_FILL',

	fetchWeeklySlot: (data = null) => ({
		type: actions.FETCH_SLOT,
		payload: { data }
	}),

	fetchWeeklySlotSuccess: (data = null) => ({
		type: actions.FETCH_SLOT_SUCCESS,
		payload: { data }
	}),

	fetchWeeklySlotError: (data = null) => ({
		type: actions.FETCH_SLOT_ERROR,
		payload: { data }
	}),

	selectAppointment: (data) => ({
		type: actions.SELECT_APPOINTMENT,
		payload: { data }
	}),

	bookAppointment: (data) => ({
		type: actions.BOOK_APPOINTMENT,
		payload: { data }
	}),

	bookAppointmentError: (data) => ({
		type: actions.BOOK_APPOINTMENT_ERROR,
		payload: { data }
	}),

	bookAppointmentSuccess: () => ({
		type: actions.BOOK_APPOINTMENT_SUCCESS
	}),

	setModal: (data) => ({
		type: actions.MODAL_HANDLE,
		payload: { data }
	}),

	resetState: () => ({
		type: actions.RESET_STATE
	})
};

export default actions;
