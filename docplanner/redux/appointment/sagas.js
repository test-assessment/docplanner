import { all, takeEvery, put, call, select } from 'redux-saga/effects';

import actions from './actions';
import apiAppointment from './api';

import moment from 'moment';

export const appState = (state) => state.App;

export function* getSlots({ payload }) {
	try {
		const { date, forwardWeeks } = payload.data;

		const momentDate = moment(date).startOf('isoweek');

		if (momentDate) {
			let weekCalls = [];

			for (let i = 0; i < (parseInt(forwardWeeks) || 1); i++) {
				weekCalls.push(call(apiAppointment.getWeeklySlotList, momentDate.add(i, 'week').format('YYYYMMDD')));
			}

			const callYield = yield all(weekCalls);

			if (callYield.length) {
				yield put(actions.fetchWeeklySlotSuccess(callYield.reduce((acc, cur) => [ ...acc, ...cur ])));
			} else {
				yield put(actions.fetchWeeklySlotError('Items not loaded'));
			}
		} else {
			yield put(actions.fetchWeeklySlotError('Wrong date'));
		}
	} catch (error) {
		yield put(actions.fetchWeeklySlotError('Cannot connect to server'));
	}
}

export function* saveAppointment({ payload }) {
	try {
		const { user } = yield select(appState);
		const { End, Start, Notes } = payload.data;

		if (!user) {
			yield put(actions.bookAppointmentError('User not exists'));
		}

		if (!End || !Start || !Notes) {
			yield put(actions.bookAppointmentError('Missing information'));
		}

		const appointmentObj = {
			Start: moment(Start).format('YYYY-MM-DD HH:mm:ss'),
			End: moment(End).format('YYYY-MM-DD HH:mm:ss'),
			Comments: Notes,
			Patient: user
		};

		const response = yield call(apiAppointment.saveAppointment, appointmentObj);

		if (response) {
			yield put(actions.bookAppointmentSuccess());
		} else {
			yield put(actions.bookAppointmentError('Error saving Item'));
		}
	} catch (error) {
		yield put(actions.bookAppointmentError(error));
	}
}

export default function* rootSaga() {
	yield all([ takeEvery(actions.FETCH_SLOT, getSlots), takeEvery(actions.BOOK_APPOINTMENT, saveAppointment) ]);
}
