import sFetch from '@shared/common/lib/helpers/sFetch';

class apiAppointment {
	saveAppointment = async (payload) => {
		return await sFetch
			.post(`${process.env.API_URL}/availability/BookSlot`, payload)
			.then((response) => {
				return response;
			})
			.catch((error) => ({ error: JSON.stringify(error) }));
	};

	getWeeklySlotList = async (date) => {
		return await sFetch
			.get(`${process.env.API_URL}/availability/GetWeeklySlots/${date}`)
			.then((response) => {
				return response;
			})
			.catch((error) => ({ error: JSON.stringify(error) }));
	};
}

export default new apiAppointment();
