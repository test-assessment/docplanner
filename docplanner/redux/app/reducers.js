import produce from 'immer';
import actions from './actions';
import moment from 'moment';

const initState = {
	lang: 'en',

	// This "user" for sure must be in another reducer but not here. Is just for this assessment.
	user: {
		Name: 'Jordan',
		SecondName: 'Walke',
		Email: 'jordan.walke@front.it',
		Phone: '+39 6532 5514'
	}
};

const appReducer = (state = initState, { type, payload }) =>
	produce(state, (draft) => {
		switch (type) {
			case actions.SET_LANG: {
				moment.locale(payload.data || 'en');
				draft.lang = payload.data;
				break;
			}
			// More cases...
		}
	});

export default appReducer;
