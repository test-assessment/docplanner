import produce from 'immer';

import appReducer from '../reducers';
import actions from '../actions';

/* eslint-disable default-case, no-param-reassign */
describe('App Reducer', () => {
	let state;

	beforeEach(() => {
		state = {
			lang: 'en',
			user: {
				Name: 'Jordan',
				SecondName: 'Walke',
				Email: 'jordan.walke@front.it',
				Phone: '+39 6532 5514'
			}
		};
	});

	it('Should return the initial state', () => {
		const expectedResult = state;

		expect(appReducer(undefined, {})).toEqual(expectedResult);
	});

	it('Should change language', () => {
		const lang = 'es';

		const expectedResult = produce(state, (draft) => {
			draft.lang = lang;
		});

		expect(appReducer(state, actions.changeLang(lang))).toEqual(expectedResult);
	});
});
