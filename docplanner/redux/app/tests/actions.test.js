import actions from '../actions';

describe('App Actions', () => {
	describe('Lang Action', () => {
		it('Should return the correct type', () => {
			const expectedResult = {
				type: actions.SET_LANG,
				payload: {
					data: 'en'
				}
			};

			expect(actions.changeLang()).toEqual(expectedResult);
		});

		it('Should return other language', () => {
			const testLang = 'es';

			const expectedResult = {
				type: actions.SET_LANG,
				payload: {
					data: testLang
				}
			};

			expect(actions.changeLang(testLang)).toEqual(expectedResult);
		});
	});
});
