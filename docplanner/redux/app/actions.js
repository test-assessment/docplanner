const DOCUMENT = 'APP_';

const actions = {
	SET_LANG: DOCUMENT + 'SET_LANG',

	changeLang: (data = 'en') => ({
		type: actions.SET_LANG,
		payload: { data }
	})
};

export default actions;
