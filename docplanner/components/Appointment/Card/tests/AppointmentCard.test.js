import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import { render, waitFor, screen } from '@testing-library/react';

import AppointmentCard from '../AppointmentCard';
import { IntlProvider } from 'react-intl';
import AppLocale from '@shared/config/translation';
import { ConfigProvider } from 'antd';

import moment from 'moment';

const renderer = new ShallowRenderer();

beforeEach(() => {
	require('mutationobserver-shim');
});

describe('<AppointmentCard />', () => {
	it('Should render and match the snapshot', () => {
		const appointmentData = [
			{ key: 'professional', value: 'Simeon Molas Ramos' },
			{ key: 'date', value: moment('2020-08-19T11:50:00').format('dddd, Do MMMM  YYYY, h:mm a') },
			{ key: 'location', value: "Ps. de I' Estació, 12 (Bajos) 43800 Valls, Terragona" }
		];

		renderer.render(
			<ConfigProvider locale={AppLocale['en'].antd}>
				<IntlProvider locale={AppLocale['en'].locale} messages={AppLocale['en'].messages}>
					<AppointmentCard appointmentData={appointmentData} />
				</IntlProvider>
			</ConfigProvider>
		);

		const renderedOutput = renderer.getRenderOutput();

		expect(renderedOutput).toMatchSnapshot();
	});

	it('Should has class attribute', () => {
		const appointmentData = [
			{ key: 'professional', value: 'Simeon Molas Ramos' },
			{ key: 'date', value: moment('2020-08-19T11:50:00').format('dddd, Do MMMM  YYYY, h:mm a') },
			{ key: 'location', value: "Ps. de I' Estació, 12 (Bajos) 43800 Valls, Terragona" }
		];

		const { container: { firstChild } } = render(
			<ConfigProvider locale={AppLocale['en'].antd}>
				<IntlProvider locale={AppLocale['en'].locale} messages={AppLocale['en'].messages}>
					<AppointmentCard appointmentData={appointmentData} />
				</IntlProvider>
			</ConfigProvider>
		);

		expect(firstChild.hasAttribute('class')).toBe(true);
	});

	it('Should render and have profesional name', async () => {
		const appointmentData = [
			{ key: 'professional', value: 'Simeon Molas Ramos' },
			{ key: 'date', value: moment('2020-08-19T11:50:00').format('dddd, Do MMMM  YYYY, h:mm a') },
			{ key: 'location', value: "Ps. de I' Estació, 12 (Bajos) 43800 Valls, Terragona" }
		];

		const { getByText } = render(
			<ConfigProvider locale={AppLocale['en'].antd}>
				<IntlProvider locale={AppLocale['en'].locale} messages={AppLocale['en'].messages}>
					<AppointmentCard appointmentData={appointmentData} />
				</IntlProvider>
			</ConfigProvider>
		);

		expect(getByText('Simeon Molas Ramos')).toBeTruthy();
	});
});
