import React from 'react';

import { InfoRow, CardWrapper, Icon, Info } from './AppointmentCard.styles';
import Card from '@shared/components/UI/elements/Card';

import { useIntl } from 'react-intl';
import DateDetail from '@shared/common/interface/Date/DateDetail';

const fieldData = {
	professional: {
		icon: 'person',
		translationKey: 'appointment.card.placeholder.professional'
	},
	date: {
		icon: 'calendar-outline',
		translationKey: 'appointment.card.placeholder.date'
	},
	location: {
		icon: 'location',
		translationKey: 'appointment.card.placeholder.location'
	}
};

const AppointmentCard = (props) => {
	const intl = useIntl();

	const showInfo = (cardInfo) => {
		return cardInfo.map(
			(item, index) =>
				item.hasOwnProperty('value') &&
				item.value.length > 0 && (
					<InfoRow key={index} data-cy={'cardInfo' + index}>
						<Icon>
							<ion-icon name={fieldData[item.key].icon} />
						</Icon>
						<Info>
							<DateDetail
								title={intl.formatMessage({ id: fieldData[item.key].translationKey })}
								date={item.value}
							/>
						</Info>
					</InfoRow>
				)
		);
	};

	return (
		props.appointmentData && (
			<Card>
				<CardWrapper>{showInfo(props.appointmentData)}</CardWrapper>
			</Card>
		)
	);
};

export default AppointmentCard;
