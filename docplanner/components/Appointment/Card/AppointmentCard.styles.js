import styled from 'styled-components';

const CardWrapper = styled.div`
	.ant-card {
		margin-bottom: 15px;
		min-height: 180px;
		display: inline-block;
		position: relative;
		width: 100%;
		border: 0;

		.ant-card-body {
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
			width: 60%;
			display: inline-block;
			padding: 15px;
			vertical-align: top;
			text-align: left;

			div {
				text-overflow: ellipsis;
				white-space: nowrap;
				overflow: hidden;
			}
		}
	}
`;

const InfoRow = styled.div`
	text-align: left;
	padding: 8px;

	@media only screen and (max-width: 768px) {
		text-align: center;
	}

	&:hover {
		background-color: #e9f4fb;
	}

	.ant-card {
		padding: 4px;
		width: 100%;
		min-height: 180px;
		display: inline-block;
		position: relative;
		border: 0;
	}
`;

const Icon = styled.div`
	display: inline-block;
	vertical-align: middle;
	color: #747474;
	text-align: left;
	margin: 8px 12px;

	@media only screen and (max-width: 768px) {
		width: 100%;
		text-align: center;
		margin: 10px 0;
	}

	ion-icon {
		color: #848484;
		font-size: 28px;
	}

	&.blue {
		ion-icon {
			color: #3198d9 !important;
		}
	}
`;

const Info = styled.div`
	display: inline-block;
	vertical-align: middle;
	color: #333333;
	font-size: 14px;
	text-align: center;

	@media only screen and (min-width: 768px) {
		text-align: left;
	}

	.title {
		color: #4e4e4e;
		font-size: 13px;
	}

	.description {
		font-weight: 500;
		font-size: 15px;
	}
`;

export { InfoRow, CardWrapper, Icon, Info };
