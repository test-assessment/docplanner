import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import LayoutContentWrapper from '@shared/components/UI/elements/LayoutContentWrapper';
import Box from '@shared/components/UI/elements/Box';

import { Holder } from './AppointmentContainer.styles';

import { Row, Col } from 'antd';

import AppointmentCard from '@shared/components/Appointment/Card/AppointmentCard';
import RescheduleContainer from '@shared/components/Schedule/RescheduleContainer';
import Button from '@shared/common/interface/Button/Button';
import LangBox from '@shared/components/UI/translation/LangBox';

import { useIntl } from 'react-intl';

import AppointmentActions from '@shared/redux/appointment/actions';

import moment from 'moment';

import SuccessInfoCard from '@shared/components/Schedule/InformationCard/SuccessInfoCard';
import ErrorInfoCard from '@shared/components/Schedule/InformationCard/ErrorInfoCard';

const { resetState, fetchWeeklySlot } = AppointmentActions;

export default function AppointmentContainer(props) {
	const { appointment, modalActive, errorMessage } = useSelector((state) => state.appointment);

	const [ resetRescheduleState, setResetRescheduleState ] = useState(false);

	const intl = useIntl();
	const dispatch = useDispatch();

	const appointmentData = [
		{ key: 'professional', value: 'Simeon Molas Ramos' },
		{ key: 'date', value: moment('2020-08-19T11:50:00').format('dddd, Do MMMM  YYYY, h:mm a') },
		{ key: 'location', value: "Ps. de I' Estació, 12 (Bajos) 43800 Valls, Terragona" }
	];

	/*
		This could be deleted with the button that calls this method (fetchSlots).
		Is for testing only
	*/
	const today = moment();
	const todayStr = today.format('YYYYMMDD');

	const fetchSlots = () => {
		setResetRescheduleState(!resetRescheduleState);
		dispatch(fetchWeeklySlot({ date: todayStr, forwardWeeks: today.day() === 1 ? 1 : 2 }));
	};
	/*
		End of testing code
	*/

	const showSuccess = Object.keys(appointment).length > 0 && modalActive === false && errorMessage === false;
	const showError = errorMessage !== false;

	const formatNewAppointmentDate = () => {
		if (appointment) {
			return moment(appointment.Start).format('dddd, Do MMMM  YYYY, h:mm a');
		}
		return '';
	};

	return (
		<LayoutContentWrapper>
			<Box>
				<Holder>
					<Row>
						<Col xs={24}>
							<Button onClick={() => dispatch(resetState())}>Reset State</Button>{' '}
							<Button onClick={() => fetchSlots()}>Fetch slots</Button>
						</Col>
					</Row>
					<Row>
						<Col xs={24}>
							<LangBox />
						</Col>
					</Row>

					{showError && <ErrorInfoCard />}

					{showSuccess && (
						<SuccessInfoCard
							appointmentData={appointmentData}
							newAppointmentDate={formatNewAppointmentDate()}
						/>
					)}
				</Holder>

				{!showError &&
				!showSuccess && (
					<Holder>
						<Row>
							<Col xs={24} style={{ margin: '20px 0px' }}>
								<h2>
									{intl.formatMessage({ id: 'appointment.header.confirm' })}{' '}
									<b>{appointmentData[0].value}</b>.
								</h2>
							</Col>
							<Col xs={24} gutter={40}>
								<AppointmentCard appointmentData={appointmentData} />
							</Col>
						</Row>
						<Row>
							<Col className="new-event" xs={24} style={{ margin: '20px 0px' }} >
								<h2>{intl.formatMessage({ id: 'reschedule.title' })}</h2>
								<h2>{intl.formatMessage({ id: 'reschedule.subtitle' })}</h2>
							</Col>
						</Row>
						<Row>
							<Col xs={24}>
								<RescheduleContainer
									schedule={props.schedule}
									appointmentData={appointmentData}
									boolResetRescheduleState={resetRescheduleState}
								/>
							</Col>
						</Row>
					</Holder>
				)}
			</Box>
		</LayoutContentWrapper>
	);
}
