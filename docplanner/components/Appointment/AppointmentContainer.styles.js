import styled from 'styled-components';
import { palette } from 'styled-theme';

const Holder = styled.div`
	display: inline-block;
	text-align: center;
	width: 100%;
	text-align: center;
	margin: auto;

	min-width: 100%;

	.ant-row {
		margin: 0 0 16px 0;
	}

	button {
		@media only screen and (max-width: 768px) {
			width: 100%;
			margin: 5px 0;
		}
	}

	.new-event {
		text-align: left;

		h2:first-child {
			font-weight: bold;
		}
	}
`;

export { Holder };
