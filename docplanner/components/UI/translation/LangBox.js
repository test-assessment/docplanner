import React from 'react';
import { useDispatch } from 'react-redux';

import Select, { SelectOption } from '@shared/common/interface/Select/Select';

import AppActions from '@shared/redux/app/actions';

const { changeLang } = AppActions;

const Option = SelectOption;

export default function LangBox() {
	const dispatch = useDispatch();

	const handleChange = (e) => {
		dispatch(changeLang(e));
	};

	return (
		<Select style={{ width: '100%' }} placeholder="Lang" onChange={(e) => handleChange(e)}>
			<Option key="es" value="es">
				Spanish
			</Option>
			<Option key="en" value="en">
				English
			</Option>
		</Select>
	);
}
