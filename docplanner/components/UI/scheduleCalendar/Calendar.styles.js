import styled from 'styled-components';
import { borderRadius } from '@shared/common/lib/helpers/styleHelper';

const DateSelect = styled.div`
	display: inline-table;
	padding: 4px;

	.date-select-text {
		${borderRadius('4px')};
		-webkit-text-decoration-line: underline;
		text-decoration-line: underline;
		font-weight: bold;
		background-color: #e9f4fb;
		color: #2382ff;
		padding: 4px;
		width: 80px;
		cursor: pointer;
	}
`;

const NoDatesWrapper = styled.div`
	padding: 40px;
	font-size: 20px;
`;

export { DateSelect, NoDatesWrapper };
