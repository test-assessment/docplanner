import React, { useState } from 'react';

import moment from 'moment';

import { DateSelect, NoDatesWrapper } from './Calendar.styles';

import { decodeDate } from '@shared/common/lib/helpers/dateHelper';

import CalendarTable from './CalendarTable';

import { useIntl } from 'react-intl';

export default function Calendar(props) {
	const intl = useIntl();

	const ITEM_LIMIT = 8;

	const [ limit, setLimit ] = useState(ITEM_LIMIT);

	React.useEffect(
		() => {
			setLimit(ITEM_LIMIT);
		},
		[ props.schedule ]
	);

	const selectDateTimeHandler = (dateTime) => {
		props.funcSelectDateTime(dateTime);
	};

	const paginateMore = () => {
		setLimit(0);
	};

	const orderedByDate = (scheduleDates) =>
		scheduleDates.slice().sort(function(b, a) {
			return new Date(decodeDate(b.Start)) - new Date(decodeDate(a.Start));
		});

	const getTable = (scheduleDates) => {
		return orderedByDate(scheduleDates).reduce((acc, dateTime, index) => {
			if (dateTime.Taken) {
				return acc;
			}

			const dateMoment = moment(dateTime.Start);

			if (!dateMoment) {
				return acc;
			}

			let dayWeek = dateMoment.format('YYYYMMDD');

			if (!acc[dayWeek]) {
				acc[dayWeek] = {
					day: dateMoment.format('DD MMM'),
					times: []
				};
			}

			if (limit === 0 || acc[dayWeek].times.length < limit) {
				acc[dayWeek].times.push(
					<DateSelect key={'week' + dayWeek + '-' + index} onClick={() => selectDateTimeHandler(dateTime)}>
						<div className="date-select-text">{dateMoment.format('HH:mm')}</div>
					</DateSelect>
				);
			}

			return acc;
		}, {});
	};

	const getCalendarTable = () => {
		if (props.schedule && props.schedule.length > 0) {
			const dayDates = getTable(props.schedule);

			return (
				<CalendarTable
					dates={dayDates}
					funcPaginate={paginateMore}
					funcFetchSlot={props.funcFetchSlot}
					selectedDate={props.selectedDate}
					itemLimit={limit}
				/>
			);
		} else {
			return <NoDatesWrapper>{intl.formatMessage({ id: 'calendar.no.dates' })}</NoDatesWrapper>;
		}
	};

	return getCalendarTable();
}
