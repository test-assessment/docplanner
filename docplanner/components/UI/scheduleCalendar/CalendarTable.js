import React, { useState } from 'react';

import { CalendarTableWrapper, CalendarTableControlWrapper } from './CalendarTable.styles';
import { DateSelect } from './Calendar.styles';

import { useIntl } from 'react-intl';

import CalendarControls from './CalendarControls';

import moment from 'moment';

export default function CalendarTable(props) {
	const intl = useIntl();

	const [ paginateButton, setPaginateButton ] = useState(true);

	React.useEffect(
		() => {
			setPaginateButton(true);
		},
		[ props.selectedDate ]
	);

	const weekDays = Array.apply(null, Array(7)).map(function(_, i) {
		return moment(i, 'e').startOf('week').isoWeekday(i).format('dddd');
	});

	const sortWeekDays = () => {
		var currentDay = new Date().getDay();

		return weekDays.slice(currentDay).concat(weekDays.slice(0, currentDay));
	};

	const weekColumns = () => {
		const columns = sortWeekDays().map((day, index) => {
			const headerDate = moment(props.selectedDate).add(index, 'days');
			const dateIndex = headerDate.format('YYYYMMDD');

			return (
				<div className="column" key={'calendarTableColumn' + index}>
					<div className="header" data-cy="dateCalendarHeader">
						<p>{day.charAt(0).toUpperCase() + day.slice(1)}</p>
						<span>{headerDate.format('DD MMM')}</span>
					</div>

					{props.dates[dateIndex] && (
						<div className="body" data-cy="dateCalendarBody">
							{props.dates[dateIndex].times}

							{paginateButton &&
							props.dates[dateIndex].times.length >= props.itemLimit && (
								<DateSelect>
									<div
										className="date-select-text"
										onClick={() => {
											props.funcPaginate();
											setPaginateButton(false);
										}}
									>
										{intl.formatMessage({ id: 'calendar.view.more' })}
									</div>
								</DateSelect>
							)}
						</div>
					)}
				</div>
			);
		});

		return columns;
	};

	return (
		<React.Fragment>
			<CalendarTableControlWrapper>
				<CalendarControls funcFetchSlot={props.funcFetchSlot} selectedDate={props.selectedDate} />
			</CalendarTableControlWrapper>

			<CalendarTableWrapper>{weekColumns()}</CalendarTableWrapper>
		</React.Fragment>
	);
}
