import styled from 'styled-components';

const CalendarTableWrapper = styled.div`
	display: flex;
	flex-direction: row;
	min-height: 440px;
	overflow-x: auto;

	@media only screen and (min-width: 768px) {
		overflow: hidden;
	}

	.column {
		flex: 1 0 14.28%;

		@media only screen and (max-width: 767px) {
			width: 100%;
			flex: none;
		}

		.header {
			font-weight: bold;
			border-bottom: 1px solid #ededed;
			padding: 14px;

			p {
				margin: 0;
			}

			span {
				font-weight: initial;
			}
		}

		.body {
		}
	}
`;

const CalendarTableControlWrapper = styled.div``;

export { CalendarTableWrapper, CalendarTableControlWrapper };
