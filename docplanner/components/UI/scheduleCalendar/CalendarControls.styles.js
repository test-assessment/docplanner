import styled from 'styled-components';
import { nonSelecteable } from '@shared/common/lib/helpers/styleHelper';

const Wrapper = styled.div`
	width: 100%;
	height: 110px;
	border-bottom: 1px solid #e0e4e4;
	vertical-align: middle;
	padding: 30px 0;

	.title {
		font-size: 18px;
		line-height: 55px;
		color: #333333;
		font-weight: 500;
		${nonSelecteable()};

		@media only screen and (min-width: 768px) {
			font-size: 24px;
			line-height: 45px;
		}
	}

	.arrow {
		font-size: 40px;
		color: #2382ff;
		cursor: pointer;
		width: 40px;
		height: 1px;

		&.left {
			float: left;
			margin-left: 14px;
		}

		&.right {
			float: right;
			margin-right: 14px;
			cursor: ;
		}
	}
`;

export { Wrapper };
