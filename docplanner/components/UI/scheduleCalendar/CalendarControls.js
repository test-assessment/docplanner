import React from 'react';

import moment from 'moment';

import { Wrapper } from './CalendarControls.styles';

export default function CalendarControls(props) {
	const today = moment().format('YYYYMMDD');

	return (
		<Wrapper>
			<span className="arrow left">
				{props.selectedDate > today && (
					<ion-icon name="chevron-back-outline" onClick={() => props.funcFetchSlot('prev')} />
				)}
			</span>

			<span className="title">
				{moment(props.selectedDate).format('DD/MM/YYYY')} -{' '}
				{moment(props.selectedDate).add(6, 'days').format('DD/MM/YYYY')}
			</span>

			<span className="arrow right">
				<ion-icon name="chevron-forward-outline" onClick={() => props.funcFetchSlot('next')} />
			</span>
		</Wrapper>
	);
}
