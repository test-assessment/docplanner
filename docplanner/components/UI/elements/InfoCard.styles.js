import styled from 'styled-components';
import { borderRadius } from '@shared/common/lib/helpers/styleHelper';

const InfoCardWrapper = styled.div`
	height: 370px;
	position: relative;

	.color-fill {
		height: 104px;

		&.success {
			background-color: #baffd6;
		}
		&.error {
			background-color: #fcedec;
		}
		&.info {
			background-color: #d8ecff;
		}
	}

	.main-icon {
		font-size: 130px;
		position: absolute;
		top: 100px;
		left: 50%;
		transform: translate(-50%, -50%);
		line-height: 0;

		&.info {
			ion-icon {
				color: #319bff;
			}
		}
		&.success {
			ion-icon {
				color: #3fc380;
			}
		}
		&.error {
			ion-icon {
				color: #e74c3c;
			}
		}
	}

	.empty-skeleton-container {
		position: absolute;
		top: 100px;
		left: 50%;
		transform: translate(-50%, -50%);
		line-height: 0;

		.empty-skeleton {
			width: 100px;
			height: 100px;
			background-color: white;
			border-radius: 50%;
			${borderRadius('50%')};
		}
	}

	.content {
		padding: 70px 10px 20px 10px;

		.title {
			font-size: 24px;
			font-weight: bold;
		}

		.information {
		}
	}
`;

export { InfoCardWrapper };
