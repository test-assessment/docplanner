import styled from 'styled-components';
import { borderRadius } from '@shared/common/lib/helpers/styleHelper';

const CardWrapper = styled.div`
	${borderRadius('8px')};
	background-color: #fff;
	border: #e0e4e4 solid 1px;
`;

export { CardWrapper };
