import styled from 'styled-components';

const LayoutContentStyle = styled.div`
	width: 100%;
`;

export default LayoutContentStyle;
