import React from 'react';
import { BoxWrapper } from './Box.styles';

export default function Box(props) {
	return <BoxWrapper style={props.style}>{props.children}</BoxWrapper>;
}
