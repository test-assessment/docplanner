import React from 'react';
import { LayoutContentWrapper } from './LayoutContentWrapper.styles';

const LayoutWrapper = (props) => (
	<LayoutContentWrapper className={'LayoutContentWrapper'} {...props}>
		{props.children}
	</LayoutContentWrapper>
);

export default LayoutWrapper;
