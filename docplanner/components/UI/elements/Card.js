import React from 'react';
import { CardWrapper } from './Card.styles';

export default function Card(props) {
	return <CardWrapper style={props.style}>{props.children}</CardWrapper>;
}
