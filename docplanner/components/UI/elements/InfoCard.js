import React from 'react';

import Card from '@shared/components/UI/elements/Card';
import { InfoCardWrapper } from './InfoCard.styles';

export default function InfoCard(props) {
	const iconName = props.icon || 'alert-circle';
	const type = props.type || 'info';

	return (
		<Card>
			<InfoCardWrapper>
				<div className={'color-fill ' + type} />
				<div className="empty-skeleton-container">
					<div className="empty-skeleton" />
				</div>
				<div className={'main-icon ' + type}>
					<ion-icon name={iconName} />
				</div>
				<div className="content">
					<div className="title">{props.title || ''}</div>
					<div className="information">{props.children}</div>
				</div>
			</InfoCardWrapper>
		</Card>
	);
}
