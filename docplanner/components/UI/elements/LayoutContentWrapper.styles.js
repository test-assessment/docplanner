import styled from 'styled-components';

const LayoutContentWrapper = styled.div`
	max-width: 900px;
	margin: auto;
	
	padding: 15px;
	text-align: center;
	display: flex;
	flex-flow: row wrap;

	@media only screen and (max-width: 767px) {
		//padding: 50px 20px;
	}

	@media (max-width: 580px) {
		//padding: 15px;
	}
`;

export { LayoutContentWrapper };
