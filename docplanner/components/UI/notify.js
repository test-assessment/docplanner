import { notification } from 'antd';

const createNotification = (type, message, description) => {
	notification[type]({
		message,
		description,
		placement: 'topRight'
	});
};

export default createNotification;
