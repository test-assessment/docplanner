import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Card from '@shared/components/UI/elements/Card';
import Calendar from '@shared/components/UI/scheduleCalendar/Calendar';

import { AppointmentSchedule } from './RescheduleContainer.styles';

import AppointmentActions from '@shared/redux/appointment/actions';

import moment from 'moment';

import RescheduleModal from '@shared/components/Schedule/Modal/RescheduleModal';

const { fetchWeeklySlot, selectAppointment, bookAppointment } = AppointmentActions;

export default function RescheduleContainer(props) {
	const { slot, appointment, modalActive, errorMessage, isLoading } = useSelector((state) => state.appointment);

	const today = moment();
	const todayStr = today.format('YYYYMMDD');

	const [ selectedDate, setSelectedDate ] = useState(todayStr);

	const dispatch = useDispatch();

	useEffect(
		() => {
			// Here when we ask for week slots. If we are not on Monday, we need to
			// ask two weeks to fullfil the calendar dates
			dispatch(fetchWeeklySlot({ date: selectedDate, forwardWeeks: today.day() === 1 ? 1 : 2 }));
		},
		[ selectedDate ]
	);

	useEffect(
		() => {
			setSelectedDate(todayStr);
		},
		[ props.boolResetRescheduleState ]
	);

	const fetchWeeklySlotHandler = (option) => {
		let newDate = selectedDate;

		if (option === 'prev') {
			newDate = moment(selectedDate).subtract(1, 'week').format('YYYYMMDD');
		} else if (option === 'next') {
			newDate = moment(selectedDate).add(1, 'week').format('YYYYMMDD');
		}

		setSelectedDate(newDate < todayStr ? todayStr : newDate);
	};

	const selectDateTimeHandler = (select) => {
		dispatch(selectAppointment(select));
	};

	const appointmentDate = () => {
		if (props.appointmentData) {
			const dateObj = props.appointmentData.filter((item) => item.key === 'date');

			if (dateObj && dateObj[0]) {
				return dateObj[0].value;
			}
		}

		return '';
	};

	const rescheduleConfirm = () => {
		dispatch(bookAppointment({ ...appointment, Notes: 'All good doc.' }));
	};

	const showSuccess = Object.keys(appointment).length > 0 && modalActive === false && errorMessage === false;
	const showError = errorMessage !== false;

	return (
		<AppointmentSchedule>
			<RescheduleModal
				modalState={modalActive}
				newAppointmentDate={appointment}
				appointmentDate={appointmentDate()}
				funcRescheduleConfirm={rescheduleConfirm}
				isLoading={isLoading}
			/>

			{!showError &&
			!showSuccess && (
				<Card>
					{/* If you want a loader... remember get 'isLoading' from state
					{isLoading && (
						<LoaderWrapper>
							<Skeleton active paragraph={{ rows: 6 }} />
						</LoaderWrapper>
					)} */}

					<Calendar
						schedule={slot}
						selectedDate={selectedDate}
						funcSelectDateTime={selectDateTimeHandler}
						funcFetchSlot={fetchWeeklySlotHandler}
					/>
				</Card>
			)}
		</AppointmentSchedule>
	);
}
