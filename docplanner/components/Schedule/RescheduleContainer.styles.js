import styled from 'styled-components';

const AppointmentSchedule = styled.div`
	table {
		width: 100%;
	}
`;

export { AppointmentSchedule };
