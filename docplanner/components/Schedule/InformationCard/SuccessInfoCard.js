import React from 'react';
import { useIntl } from 'react-intl';

import InfoCard from '@shared/components/UI/elements/InfoCard';

export default function SuccessInfoCard(props) {
	const intl = useIntl();

	return (
		<InfoCard
			icon="checkmark-circle"
			type="success"
			title={intl.formatMessage({ id: 'reschedule.result.success.title' })}
		>
			<div style={{ fontSize: '16px' }}>
				<p style={{ marginBottom: '0' }}>
					<b>{props.appointmentData[0].value}</b>
				</p>
				<p
					style={{
						textDecoration: 'line-through',
						color: '#cdcdcd',
						fontWeight: '600',
						marginBottom: '0'
					}}
				>
					{props.appointmentData[1].value}
				</p>
				<p style={{ marginBottom: '0' }}>
					<b>{props.newAppointmentDate}</b>
				</p>
				<p style={{ marginBottom: '0' }}>{props.appointmentData[2].value}</p>
			</div>
		</InfoCard>
	);
}
