import React from 'react';
import { useIntl } from 'react-intl';

import InfoCard from '@shared/components/UI/elements/InfoCard';

export default function ErrorInfoCard(props) {
	const intl = useIntl();

	return (
		<InfoCard icon="close-circle" type="error" title={intl.formatMessage({ id: 'reschedule.result.fail.title' })}>
			<div style={{ fontSize: '16px' }}>
				<p style={{ marginBottom: '0', fontWeight: '600' }}>
					{intl.formatMessage({ id: 'reschedule.result.fail.subtitle' })}
				</p>
			</div>
		</InfoCard>
	);
}
