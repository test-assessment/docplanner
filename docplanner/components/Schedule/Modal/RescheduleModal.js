import React from 'react';
import { useDispatch } from 'react-redux';

import Modals from '@shared/components/Feedback/Modal';

import ModalStyle from './RescheduleModal.styles';

import DateDetail from '@shared/common/interface/Date/DateDetail';

import moment from 'moment';

import { useIntl } from 'react-intl';

import AppointmentActions from '@shared/redux/appointment/actions';

const Modal = ModalStyle(Modals);

const { setModal } = AppointmentActions;

export default function RescheduleModal(props) {
	const intl = useIntl();

	const dispatch = useDispatch();

	const handleOk = () => {
		props.funcRescheduleConfirm();
	};

	const handleCancel = () => {
		dispatch(setModal(false));
	};

	const strikeDate = <span style={{ textDecoration: 'line-through' }}>{props.appointmentDate}</span>;
	const newDate =
		props.newAppointmentDate && props.newAppointmentDate.Start
			? moment(props.newAppointmentDate.Start).format('dddd, Do MMMM  YYYY, HH:mm a')
			: '--';

	return (
		<Modal
			visible={props.modalState}
			onCancel={handleCancel}
			title={intl.formatMessage({ id: 'modal.reschedule.confirm.header' })}
			okText={intl.formatMessage({ id: 'appointment.options.confirm' })}
			onOk={() => handleOk()}
			onClose={handleCancel}
			confirmLoading={props.isLoading}
			data-cy="rescheduleModal"
		>
			<DateDetail
				title={intl.formatMessage({ id: 'modal.reschedule.confirm.original.date' })}
				date={strikeDate}
			/>
			<hr />
			<DateDetail title={intl.formatMessage({ id: 'modal.reschedule.confirm.header.new.date' })} date={newDate} />
		</Modal>
	);
}
