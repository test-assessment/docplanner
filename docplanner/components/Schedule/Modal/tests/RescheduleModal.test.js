import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import { render, waitFor, screen } from '@testing-library/react';

import { IntlProvider } from 'react-intl';
import AppLocale from '@shared/config/translation';
import { ConfigProvider } from 'antd';

import moment from 'moment';

//import RescheduleModal from '../RescheduleModal';

const renderer = new ShallowRenderer();

beforeEach(() => {
	require('mutationobserver-shim');
});

describe('<RescheduleModal />', () => {
	it('Should render modal', () => {
		const oldDate = moment('2020-08-19T11:50:00').format('dddd, Do MMMM  YYYY, h:mm a');

		const appointment = {
			Start: '2020-10-07T09:00:00',
			End: '2020-10-07T09:10:00'
		};

		const rescheduleConfirm = () => {};

		const { getByText } = render(
			<ConfigProvider locale={AppLocale['en'].antd}>
				<IntlProvider locale={AppLocale['en'].locale} messages={AppLocale['en'].messages}>
					{/* <RescheduleModal
						modalState={true}
						newAppointmentDate={appointment}
						appointmentDate={oldDate}
						funcRescheduleConfirm={rescheduleConfirm}
						isLoading={true}
					/> */}
				</IntlProvider>
			</ConfigProvider>
		);

		// expect(getByText()).toBeTruthy();
	});
});
