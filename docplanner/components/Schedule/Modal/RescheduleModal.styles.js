import styled from 'styled-components';

const Modals = (ComponentName) => styled(ComponentName)`
    .ant-modal-header {
        padding: 13px 16px;
    }

    .ant-modal-title {
        margin: 0;
        font-size: 15px;
        line-height: 21px;
        font-weight: 500;
    }

    .ant-modal-body {
        padding: 16px;
        font-size: 13px;
        line-height: 1.5;
    }

    .ant-modal-footer {
        padding: 10px 16px 10px 10px;
        text-align: right;
        
        .ant-btn-lg {
            padding: 0 35px;
            font-size: 14px;
            height: 42px;
        }
        
        button {
            font-weight: 600;
        }

        button + button {
            background-color: #3ca263;
        }
    }

    .ant-confirm {
        .ant-modal-body {
            padding: 30px 35px;
        }
    }

    .ant-confirm-body {
        .ant-confirm-title {
            font-weight: 700;
            font-size: 15px;
        }
        
        .ant-confirm-content {
            margin-left: 42px;
            font-size: 13px;
            margin-top: 8px;
        }
    }

    hr {
        border: none;
        height: 1px;
        color: #e7e7e7;
        background-color: #e7e7e7;
        margin: 16px 0px;
    }
`;

const ModalContent = styled.div`
	p {
		font-size: 13px;
		line-height: 1.5;
	}
`;

export default Modals;

export { ModalContent };
