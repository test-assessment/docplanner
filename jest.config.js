module.exports = {
	collectCoverageFrom: [
		'docplanner/**/*.{js,jsx}',
		'!docplanner/**/*.test.{js,jsx}',
		'!docplanner/*/RbGenerated*/*.{js,jsx}',
		'!docplanner/docplanner.js',
		'!docplanner/global-styles.js',
		'!docplanner/*/*/Loadable.{js,jsx}'
	],
	setupFilesAfterEnv: [ '<rootDir>/setupTests.js' ],
	coverageThreshold: {
		global: {
			statements: 80,
			branches: 80,
			functions: 80,
			lines: 80
		}
	},
	moduleDirectories: [ 'node_modules', 'docplanner' ],
	testRegex: 'tests/.*\\.test\\.js$',
	testPathIgnorePatterns: [ '/node_modules/', '/.next/' ],
	snapshotSerializers: [],
	transformIgnorePatterns: [ '/node_modules/' ],
	transform: {
		'^.+\\.(js|jsx|ts|tsx)$': '<rootDir>/node_modules/babel-jest'
	},
	moduleNameMapper: {
		'@shared/(.*)': '<rootDir>/docplanner/$1'
	}
};
