## Doctoralia Project Summary

Live @ https://docplannerapp.herokuapp.com/

## Table of Contents

- [Objective](#objective)
- [Features](#features)
- [Tech](#tech)
- [Installation](#installation)
- [Test](#test)
- [Support](#support)

---

## Objective

Create an application for reschedule patient bookings.

## Features

- React/Redux (w/Hooks) based app.
- Responsive design
- Last stable version of all dependencies
- Support for multi language
- Supports SSR (Built with Next.js)
- Compiled with Webpack
- Organized for extension
- Tested with Jest & Cypress

## Tech
    Frontend
        For this project i’ve selected the next FE stack:
            I will use manly React Hooks for as ThoughtWorks radar recommends for this year too, is flexible and practical.
            For styling, Antd looks a proper enterprise solution for design.
            To handle async calls inside Redux, I will use Redux-Saga
            To handle persistence, Redux-persist
            Immutability with Immer
            Time with Moment.js
            For server instance management, PM2

Current repository

https://gitlab.com/test-assessment/docplanner

## Installation

### Clone

- Clone this repo: `git clone https://gitlab.com/test-assessment/docplanner`

Execute next commands (Clone repository)

git clone https://gitlab.com/test-assessment/docplanner
cd docplanner

### For Dev instance

Environment versions used:
- NodeJS 12.18.0
- NPM 6.14.4
- Yarn 1.22.4

For languages, view:
- translation\locales\en_US.json
- translation\locales\es_ES.json

First we install all dependencies:
```bash
yarn install
```

For Development, we left Next.Js running in background with SSR (Server Side Rendering)
```bash
yarn dev
```

This will setup a server at port http://localhost:3003 and an active watcher that will recompile if files are changed in real time

For Prod, first we need to compile the bundle and later execute server
```bash
yarn build && yarn startlocal
```

This will setup a server at port http://localhost:8000 and an active watcher that will recompile if files are changed in real time

### For production instance

Previous requirements on environment:
- YARN
- PM2

With YARN installed on server, install PM2

```bash
yarn global add pm2
```

Now we check that PM2 is working on bash:

```bash
pm2 list
```

If this don't work, we need to find pm2 with command 

```bash
whereis pm2
```

After that, we can use that bash location to running command, for example:

```bash
/usr/local/bin/pm2 list
```

Then, we verify that there is NO instances of MWS-Front running with this command

```bash
/usr/local/bin/pm2 list
```

If there is ANY server working, we run the command

```bash
pm2 delete {name}
```
or
```bash
/usr/local/bin/pm2 delete {name}
```

Where {name} is the name that figures on 'list' command

Then we do

```bash
yarn install
```
is for installing dependencies

After that, we need to compile the distribution '.next' instance. We can do that doing

```bash
yarn build
```

With that done, we can use the next command

```bash
yarn prod
```

That will execute a pm2 instance with the compiled production build

Then we can verify is running with

```bash
/usr/local/bin/pm2 list
```
or
```bash
pm2 list
```

More information can be found in package.json

---

## Test

To run test suit.

For Jest

```shell
$ yarn test
```

For Cypress

First build & run server with

```shell
$ yarn build && yarn startlocal
```

Then with server running at default port 8000, run:

```shell
$ yarn cypress
```

---

## Support

Any concerns about design or patterns please contact meroni.damian@gmail.com

---