beforeEach(() => {
	cy.visit('');
});

describe('When calendar is load', () => {
	describe('and has some schedule dates', () => {
		it('Date headers must be 7 (Days)', () => {
			cy.get('[data-cy="dateCalendarHeader"]').should('have.length', 7);
		});

		it('Date header must have date on every column', () => {
			cy.get('[data-cy="dateCalendarHeader"]').each(($e) => {
				expect($e).to.have.length(1);
			});
		});

		it('Each column must have valid dates', () => {
			cy.get('[data-cy="dateCalendarBody"] .date-select-text').each(($e) => {
				$e.text() !== 'See more times'
					? expect($e.text()).to.have.length(5)
					: expect($e.text(), 'See more times');
			});
		});
	});
});
