beforeEach(() => {
	cy.visit('');
});

describe('When the site loads', () => {
	describe('and a user is set on card info component', () => {
		it('Should show profesional title and name', () => {
			cy.get('[data-cy="cardInfo0"]').find('.title').should('be.visible').should('not.be.empty');
			cy.get('[data-cy="cardInfo0"]').find('.description').should('be.visible').should('not.be.empty');
		});
		it('Should show appointment title and date', () => {
			cy.get('[data-cy="cardInfo1"]').find('.title').should('be.visible').should('not.be.empty');
			cy.get('[data-cy="cardInfo1"]').find('.description').should('be.visible').should('not.be.empty');
		});
		it('Should place title and appointment address', () => {
			cy.get('[data-cy="cardInfo2"]').find('.title').should('be.visible').should('not.be.empty');
			cy.get('[data-cy="cardInfo2"]').find('.description').should('be.visible').should('not.be.empty');
		});
	});
});
